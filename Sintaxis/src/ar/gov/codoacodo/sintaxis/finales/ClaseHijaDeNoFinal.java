package ar.gov.codoacodo.sintaxis.finales;

public class ClaseHijaDeNoFinal extends ClaseNoFinal {

	@Override
	public void saludarNoFinal(String nombre) {
		System.out.println("[NO FINAL] Hello gentleman " + nombre + ", how are you?");
	}

	/**
	 * metodos finales no se pueden sobreescribir
	 */
//	@Override
//	public final void saludarFinal(String nombre) {
//		System.out.println("[FINAL] Hello partner " + nombre + ", how is it going?");
//	}

}
