package ar.gov.codoacodo.sintaxis.finales;

public class MainFinales {

	public static void main(String[] args) {
		ClaseNoFinal noFinal = new ClaseNoFinal();
		/**
		 * no se puede cambiar el valor de una variable final
		 */
		// noFinal.variableFinal = 5;
		noFinal.saludarNoFinal("Franco");
		noFinal.saludarFinal("Federico");
		ClaseHijaDeNoFinal hijaDeNoFinal = new ClaseHijaDeNoFinal();
		hijaDeNoFinal.saludarNoFinal("Juan");
		hijaDeNoFinal.saludarFinal("Carlos");
	}

}
