package ar.gov.codoacodo.sintaxis.finales;

public class ClaseNoFinal {

	public final int variableFinal = 5;

	public void saludarNoFinal(String nombre) {
		System.out.println("[NO FINAL] Hola caballero " + nombre + ", como le va?");
	}

	public final void saludarFinal(String nombre) {
		System.out.println("[FINAL] Hola compa�ero " + nombre + ", como anda usted?");
	}

}
