package ar.gov.codoacodo.sintaxis.abstractas;

/**
 * una clase abstracta no puede ser instanciada pero si sus hijos no abstractos
 * esta debe contener al menos un metodo abstracto
 */
public abstract class Pago {

	/**
	 * un metodo abstracto no puede definir su cuerpo (logica a implementar)
	 * el mismo debe ser implementado en las clases hijas no abstractas 
	 */
	public abstract void imputarPago();

	public abstract void validoPago();
	
	/**
	 * los metodos no abstractos definen su logica y todos sus hijos
	 * la heredan, salvo que la quieran sobreescribir
	 */
	public void connectarSistemaDePagos() {
		System.out.println("Conectando al sistema de pagos");
	}

}
