package ar.gov.codoacodo.sintaxis.abstractas;

/**
 * extends -> es hijo de
 * ClaseHija extends ClasePadre -> ClaseHija es hijo de ClasePadre
 */
public class PagoInvalido extends Pago {

	@Override
	public void imputarPago() {
		System.out.println("imputo el pago mal");
	}

	 @Override
	 public void validoPago() {
		 System.out.println("es invalido");
	 }

}
