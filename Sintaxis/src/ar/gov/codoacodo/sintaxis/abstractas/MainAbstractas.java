package ar.gov.codoacodo.sintaxis.abstractas;

public class MainAbstractas {

	public static void main(String[] args) {
		/**
		 * clase abstracta no puede ser instanciada, solo sus hijos no
		 * abstractos
		 */
		// Pago pago = new Pago();

		PagoTarjetaCredito pagoTC = new PagoTarjetaCredito();
		connectarSistemaDePago(pagoTC);
		validarPago(pagoTC);
		imputarPago(pagoTC);

		PagoEfectivo pagoEFT = new PagoEfectivo();
		connectarSistemaDePago(pagoEFT);
		validarPago(pagoEFT);
		imputarPago(pagoEFT);

		PagoInvalido pagoInvalido = new PagoInvalido();
		connectarSistemaDePago(pagoInvalido);
		validarPago(pagoInvalido);
		imputarPago(pagoInvalido);
	}

	private static void connectarSistemaDePago(Pago pago) {
		pago.connectarSistemaDePagos();
	}

	public static void validarPago(Pago pago) {
		pago.validoPago();
	}

	public static void imputarPago(Pago pago) {
		pago.imputarPago();
	}

}
