package ar.gov.codoacodo.sintaxis.abstractas;

/**
 * extends -> es hijo de
 * ClaseHija extends ClasePadre -> ClaseHija es hijo de ClasePadre
 */
public class PagoTarjetaCredito extends Pago {

	@Override
	public void imputarPago() {
		System.out.println("imputo pago tarjeta de credito");
	}

	@Override
	public void validoPago() {
		if (esTarjetaValida() && tieneSaldo()) {
			System.out.println("es valido TC");
		} else {
			System.out.println("es invalido TC, no tiene saldo o la TC es invalida");
		}
	}

	private boolean tieneSaldo() {
		return true;
	}

	private boolean esTarjetaValida() {
		return true;
	}

}
