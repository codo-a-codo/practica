package ar.gov.codoacodo.sintaxis.abstractas;

/**
 * extends -> es hijo de
 * ClaseHija extends ClasePadre -> ClaseHija es hijo de ClasePadre
 */
public class PagoEfectivo extends Pago {

	@Override
	public void imputarPago() {
		System.out.println("imputo pago en efectivo");
	}

	@Override
	public void validoPago() {
		if (tieneDinero()) {
			System.out.println("es valido EFT");
		} else {
			System.out.println("es invalido EFT, no tiene dinero");
		}
	}

	private boolean tieneDinero() {
		return true;
	}

}
