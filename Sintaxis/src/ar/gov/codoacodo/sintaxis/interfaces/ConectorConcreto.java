package ar.gov.codoacodo.sintaxis.interfaces;

public class ConectorConcreto implements Conector {

	@Override
	public void conectar() {
		System.out.println("--> Inicializando conexion");
		System.out.println("--> Validando puertos");
		System.out.println("--> Puertos OK");
		System.out.println("--> Estableciendo conexion");
	}

	@Override
	public void cerrarConexion() {
		System.out.println("--> Finalizando conexion");
		System.out.println("--> Liberando puertos");
		System.out.println("--> Puertos liberados");
		System.out.println("--> Conexion finalizada");
	}

}
