package ar.gov.codoacodo.sintaxis.interfaces;

public class MainIntefaces {

	public static void main(String[] args) {
		System.out.println("Conector concreto:");
		System.out.println("==================");
		ConectorConcreto conectorContreto = new ConectorConcreto();
		conectorContreto.conectar();
		conectorContreto.cerrarConexion();

		System.out.println("");

		System.out.println("Conector mockeado:");
		System.out.println("==================");
		ConectorMockeado conectorMockeado = new ConectorMockeado();
		conectorMockeado.conectar();
		conectorMockeado.cerrarConexion();
		conectorMockeado.generarMocks();
		
		System.out.println("");
		
		System.out.println("Utilizando el mockeado a traves de la interface");
		System.out.println("===============================================");
		Conector conectorMockeadoInterface = new ConectorMockeado();
		conectorMockeadoInterface.conectar();
		conectorMockeadoInterface.cerrarConexion();
		/**
		 * no se puede utilizar un metodo especifico de la clase
		 * construyendo el objeto a partir de la interface, es decir
		 * la declaracion estatica del objeto es la interface y 
		 * dicho metodo no existe en la interface
		 */
		//conectorMockeadoInterface.generarMocks();
	}

}
