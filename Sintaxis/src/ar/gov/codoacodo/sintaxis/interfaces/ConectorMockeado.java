package ar.gov.codoacodo.sintaxis.interfaces;

public class ConectorMockeado implements Conector {

	@Override
	public void conectar() {
		System.out.println("--> Conectado al servidor mockeado");
	}

	@Override
	public void cerrarConexion() {
		System.out.println("--> Conexion finalizada con el servidor mockeado");
	}

	public void generarMocks() {
		System.out.println("Generando mocks");
	}
	
}
