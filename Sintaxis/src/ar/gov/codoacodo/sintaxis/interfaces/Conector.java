package ar.gov.codoacodo.sintaxis.interfaces;

/**
 * una interface solo define variables y contratos de metodos
 * no realiza implementaciones
 */
public interface Conector {

	/**
	 * todas las variables de una interface son implicitamente
	 * publicas, estaticas y finales (public static final)
	 */
	int PUERTO_ENTRADA = 8080;
	int PUERTO_SALIDA = 8081;

	/**
	 * todos los metodos de una interface son implicitamente
	 * publicos y abstractos (public abstract)
	 */
	void conectar();

	void cerrarConexion();

}
