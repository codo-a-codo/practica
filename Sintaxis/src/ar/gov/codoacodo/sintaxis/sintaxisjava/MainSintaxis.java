package ar.gov.codoacodo.sintaxis.sintaxisjava;

//@formatter:off
/**
 * Modificadores de Visibilidad:
 * 
 * 		TIPO				PALABRA RESERVADA		DONDE ES VISIBLE
 * 1)	privado			->	private					(solo dentro de la clase)
 * 2)	paquete			->							(desde dentro de la clase y clases del mismo paquete)
 * 3)	protegido		->	protected				(desde dentro de la clase, clases del mismo paquete 
 * 													y a trav�s de las subclases - Herencia)
 * 4)	publico			->	public					(desde cualquier lado)
 * 
 */
//@formatter:on
public class MainSintaxis {

	public static void main(String[] args) {

		/**
		 * Utilizamos los 3 constructores que creamos
		 */
		Sintaxis sintaxisDemoConstructor1 = new Sintaxis();
		Sintaxis sintaxisDemoConstructor2 = new Sintaxis("Hola que tal");
		Sintaxis sintaxisDemoConstructor3 = new Sintaxis("Adios", 3);
		
		/**
		 * instancia es un objeto/instancia de la clase Sintaxis
		 */
		Sintaxis instancia = new Sintaxis();
		/**
		 * metodo con retorno (function)
		 */
		int suma = instancia.sumarNumeros(1, 2);
		System.out.println("El valor de la suma es " + suma);
		/**
		 * metodo sin retorno (procedure)
		 */
		instancia.saludarA("Pepito");
		
		int nroTelefonoCliente = instancia.nroTelefonoCliente;
		/**
		 * un atributo de instancia no puede ser llamado estaticamente (A traves de la definicion de la clase)
		 */
		// int nroTelefonoClienteStatic = Sintaxis.nroTelefonoCliente;
		
		/**
		 * las variables/metodos estaticas o de clase se acceden de la manera NombreClase.variableEstatica
		 */
		int prefijoTelefonoArgentina = instancia.prefijoTelefonoArgentina;
		/**
		 * se puede llamar a un campo/metodo estatico o de clase a traves de una instancia pero no es lo correcto
		 */
		int prefijoTelefonoArgentinaStatic = Sintaxis.prefijoTelefonoArgentina;

	}

}
