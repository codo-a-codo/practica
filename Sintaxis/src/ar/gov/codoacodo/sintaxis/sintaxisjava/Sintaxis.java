package ar.gov.codoacodo.sintaxis.sintaxisjava;

/**
 * [] opcional
 */

/**
 * modificador_visibilidad [abstracta/estatica (solo inner class)] [final] class NombreClase { }
 */
public class Sintaxis {

	/**
	 * Variables modificador_visibilidad [estatica] [final] tipoDato/Objeto nombreVariable [= valorInicial] ;
	 */
	private int variablePrivada = 0;
	int variablePaquete = 0;
	protected int variableProtegida = 0;
	public int variablePublica = 0;
	
	public int nroTelefonoCliente = 45671234;
	public static int prefijoTelefonoArgentina = 54;

	/**
	 * Constructor modificador_visibilidad [abstracta/estatica] NombreClase([tipoDatoParametro nombreParametro, ....]) { }
	 */
	public Sintaxis() {
		variablePrivada = 1;
		variablePaquete = 2;
		variableProtegida = 3;
		variablePublica = 4;
		System.out.println("Constructor vacio o default");
	}

	public Sintaxis(String mensajeBienvenida, int k) {
		System.out.println("Constructor String, int: " + mensajeBienvenida + " - " + k);
	}

	public Sintaxis(String mensaje) {
		System.out.println("Constructor String: " + mensaje);
	}


	/**
	 * Metodos 
	 * modificador_visibilidad [abstracta/estatica] [final] tipoDato/Objeto/void nombreMetodo([tipoDatoParametro nombreParametro, ....]) { }
	 */
	/**
	 * Funcion (function) -> devuelve un valor/objeto -> tipo dato
	 */
	public int sumarNumeros(int a, int b) {
		return a + b;
	}

	/**
	 * Procedimiento (procedure) -> no devuelve nada -> void
	 */
	public void saludarA(String nombre) {
		System.out.println("Hola " + nombre + "!!!");
	}
	
}
