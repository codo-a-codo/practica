package ar.gov.codoacodo.sintaxis.sintaxisjava;

public class OtraClaseMismoPaquete {

	public void pruebaVariables() {
		/**
		 * NombreClase nombreVariable = new NombreClase(); -> depende del constructor
		 */
		Sintaxis sintaxis = new Sintaxis();
		/**
		 * privado no puede ser accedido fuera de la clase
		 */
		// sintaxis.variablePrivada = 1;
		sintaxis.variablePaquete = 2;
		sintaxis.variableProtegida = 3;
		sintaxis.variablePublica = 4;
	}

}
