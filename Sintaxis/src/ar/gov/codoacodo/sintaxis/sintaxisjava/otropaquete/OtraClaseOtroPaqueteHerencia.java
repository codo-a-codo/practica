package ar.gov.codoacodo.sintaxis.sintaxisjava.otropaquete;

import ar.gov.codoacodo.sintaxis.sintaxisjava.Sintaxis;

/**
 * extends implica hereda de (hija de)
 */
public class OtraClaseOtroPaqueteHerencia extends Sintaxis {

	public void pruebaVariables() {
		/**
		 * NombreClase nombreVariable = new NombreClase(); -> depende del constructor
		 */
		OtraClaseOtroPaqueteHerencia sintaxis = new OtraClaseOtroPaqueteHerencia();
		/**
		 * privado no puede ser accedido fuera de la clase
		 */
//		 sintaxis.variablePrivada = 1;
		/**
		 * paquete no puede ser accedido fuera del paquete
		 */
//		 sintaxis.variablePaquete = 2;
		/**
		 * protegida no puede ser accedido fuera del paquete o en una clase que no herede de quien la declara
		 */
		sintaxis.variableProtegida = 3;
		sintaxis.variablePublica = 4;
	}

}
