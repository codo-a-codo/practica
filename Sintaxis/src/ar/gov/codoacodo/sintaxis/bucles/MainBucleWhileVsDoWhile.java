package ar.gov.codoacodo.sintaxis.bucles;

public class MainBucleWhileVsDoWhile {

	public static void main(String[] args) {
		bucleWhileVsDoWhile();

	}

	private static void bucleWhileVsDoWhile() {
		int i = 0;
		while (i < 0) {
			i++;
		}
		System.out.println("La variable i vale: " + i);

		int j = 0;
		do {
			j++;
		} while (j < 0);
		System.out.println("La variable j vale: " + j);
	}

}
