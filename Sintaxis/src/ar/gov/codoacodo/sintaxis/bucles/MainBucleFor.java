package ar.gov.codoacodo.sintaxis.bucles;

public class MainBucleFor {

	public static void main(String[] args) {
		System.out.println("Bucle for");
		bucleFor();
		System.out.println("Bucle for con break");
		bucleForConBreak();
		System.out.println("Bucle for con continue");
		bucleForConContinue();
	}

	private static void bucleForConContinue() {
		int[] numeros = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		for (int i = 0; i < numeros.length; i++) {
			if (numeros[i] == 5) {
				continue;
			}
			System.out.println(numeros[i]);
		}
	}

	private static void bucleForConBreak() {
		int[] numeros = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		for (int i = 0; i < numeros.length; i++) {
			if (numeros[i] > 5) {
				break;
			}
			System.out.println(numeros[i]);
		}
	}

	private static void bucleFor() {
		int[] numeros = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		for (int i = 0; i < numeros.length; i++) {
			System.out.println(numeros[i]);
		}
	}

}
