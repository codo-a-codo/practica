package ar.gov.codoacodo.sintaxis.bucles;

public class MainBucleWhile {

	public static void main(String[] args) {
		System.out.println("Bucle while");
		bucleWhile();
		System.out.println("Bucle while con break");
		bucleWhileConBreak();
		System.out.println("Bucle while con continue");
		bucleWhileConContinue();
	}

	private static void bucleWhileConContinue() {
		int i = 0;
		while (i < 10) {
			i++;
			if (i == 5) {
				continue;
			}
			System.out.println("La variable i vale: " + i);
		}
	}

	private static void bucleWhileConBreak() {
		int i = 0;
		while (i < 10) {
			i++;
			if (i > 5) {
				break;
			}
			System.out.println("La variable i vale: " + i);
		}
	}

	private static void bucleWhile() {
		int i = 0;
		while (i < 10) {
			i++;
			System.out.println("La variable i vale: " + i);
		}
	}
}
