package ar.gov.codoacodo.sintaxis.bucles;

public class MainBucleDoWhile {

	public static void main(String[] args) {
		System.out.println("Bucle while");
		bucleDoWhile();
		System.out.println("Bucle do while con break");
		bucleDoWhileConBreak();
		System.out.println("Bucle do while con continue");
		bucleDoWhileConContinue();
	}

	private static void bucleDoWhileConContinue() {
		int i = 0;
		do {
			i++;
			if (i == 5) {
				continue;
			}
			System.out.println("La variable i vale: " + i);
		} while (i < 10);
	}

	private static void bucleDoWhileConBreak() {
		int i = 0;
		do {
			i++;
			if (i > 5) {
				break;
			}
			System.out.println("La variable i vale: " + i);
		} while (i < 10);
	}

	private static void bucleDoWhile() {
		int i = 0;
		do {
			i++;
			System.out.println("La variable i vale: " + i);
		} while (i < 10);
	}

}
