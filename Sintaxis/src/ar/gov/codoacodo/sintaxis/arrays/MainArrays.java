package ar.gov.codoacodo.sintaxis.arrays;

import java.math.BigInteger;
import java.util.Scanner;

public class MainArrays {

	private static Scanner scanner;

	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		/*
		 * Un array se maneja por indices Los indices de un array van desde 0
		 * hasta N - 1 Siendo N la cantidad de elementos que contiene
		 */
		pruebaCreacionEnLinea();
		System.out.println("");
		pruebaCreacionAPedido();
		System.out.println("");
		// pruebaCreacionAPedidoUnoAUno();
		System.out.println("");
		pruebaIndices();

	}

	private static void pruebaIndices() {
		int[] array = { 10, 20, 30, 40, 50 };
		System.out.println(array[0]);
		System.out.println(array[4]);
		System.out.println(array[5]);
	}

	private static void pruebaCreacionAPedidoUnoAUno() {
		// creacion a pedido uno a uno por teclado
		int cantidadDeElementosUnoAUno = 5;
		int[] arrayDeNumerosAPedidoUnoAUno = new int[cantidadDeElementosUnoAUno];
		for (int i = 0; i < arrayDeNumerosAPedidoUnoAUno.length; i++) {
			System.out.println("Ingrese el valor del array en la posicion " + i);
			int valor = scanner.nextInt();
			arrayDeNumerosAPedidoUnoAUno[i] = valor;
		}

		for (int i = 0; i < arrayDeNumerosAPedidoUnoAUno.length; i++) {
			System.out.println(
					"Elemento en la posicion " + i + " (array[" + i + "]) vale: " + arrayDeNumerosAPedidoUnoAUno[i]);
		}
	}

	private static void pruebaCreacionAPedido() {
		// creacion a pedido
		int cantidadDeElementos = 5;
		int[] arrayDeNumerosAPedido = new int[cantidadDeElementos];
		arrayDeNumerosAPedido[0] = 101;
		arrayDeNumerosAPedido[1] = 202;
		arrayDeNumerosAPedido[2] = 303;
		arrayDeNumerosAPedido[3] = 404;
		arrayDeNumerosAPedido[4] = 505;

		for (int i = 0; i < arrayDeNumerosAPedido.length; i++) {
			System.out
					.println("Elemento en la posicion " + i + " (array[" + i + "]) vale: " + arrayDeNumerosAPedido[i]);
		}
	}

	private static void pruebaCreacionEnLinea() {
		// creacion inline
		int[] arrayDeNumerosInline = { 10, 20, 30, 40, 50 };

		for (int i = 0; i < arrayDeNumerosInline.length; i++) {
			System.out.println("Elemento en la posicion " + i + " (array[" + i + "]) vale: " + arrayDeNumerosInline[i]);
		}
	}

}
