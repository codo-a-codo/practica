package ar.gov.codoacodo.algorithms;

public class JoinPointExample {

	private static RuntimeException neverJoinEx = new RuntimeException("No hay punto de encuentro");

	public double getJoinPoint(Person p1, Person p2) {

		double origin = 0.0;

		if (p1.position < 0 || p2.position < 0) {
			throw new RuntimeException("Las posiciones no pueden ser negativas");
		}

		if (p1.velocity < 0 || p2.velocity < 0) {
			throw new RuntimeException("Los factores de velocidad no pueden ser negativos");
		}

		if (p1.position == p2.position) {
			return p1.position;
		}

		double distance = Math.abs(p1.position - p2.position);
		double distance_copy = distance;

		boolean p1PositionIsLowerThanP2Position = p1.position < p2.position;
		boolean p1PositionIsLowerThanP2Position_copy = p1PositionIsLowerThanP2Position;

		int aux1 = p1.forward ? 1 : -1;
		int aux2 = p2.forward ? 1 : -1;

		while (distance > 0) {
			p1.position += (p1.velocity * aux1);
			p2.position += (p2.velocity * aux2);
			distance = Math.abs(p1.position - p2.position);
			p1PositionIsLowerThanP2Position = p1.position < p2.position;
			if (distance >= distance_copy) {
				if (!p1.forward && !p2.forward) {
					return origin;
				}
				if (p1PositionIsLowerThanP2Position != p1PositionIsLowerThanP2Position_copy) {
					double blockSize = distance / (p1.velocity + p2.velocity);
					return p1.position + (blockSize * p2.velocity * aux2);
				}
				throw neverJoinEx;
			}
			distance_copy = distance;
			p1PositionIsLowerThanP2Position_copy = p1PositionIsLowerThanP2Position;
		}
		if (((int) p1.position) < 0) {
			p1.position = origin;
		}
		return p1.position;
	}

	public static void main(String[] args) {
		testMismaPosicion();
		testAlgunoConVelocidadNegativa();
		testAlgunoConPosicionNegativa();
		testSeAlejan();
		testHayEncuentro();
		testHayEncuentroPeroSePasaron();
		testHayEncuentroPeroSePasaron_2();
		testEncuentroEnA();
		testEncuentroEnA_2();
		testEncuentroHaciaB();
		testSeAlejanHaciaB();
	}

	private static void testSeAlejanHaciaB() {
		System.out.println("--> testSeAlejanHaciaB");
		Person p1 = new Person(2.0, 3, true);
		Person p2 = new Person(70.0, 5, true);
		test(p1, p2);
	}

	private static void testEncuentroHaciaB() {
		System.out.println("--> testEncuentroHaciaB");
		Person p1 = new Person(2.0, 3, true);
		Person p2 = new Person(70.0, 1, true);
		test(p1, p2);
	}

	private static void testEncuentroEnA_2() {
		System.out.println("--> testEncuentroEnA_2");
		Person p1 = new Person(2.0, 1, false);
		Person p2 = new Person(70.0, 3, false);
		test(p1, p2);
	}

	private static void testEncuentroEnA() {
		System.out.println("--> testEncuentroEnA");
		Person p1 = new Person(50.0, 1, false);
		Person p2 = new Person(70.0, 1, false);
		test(p1, p2);
	}

	private static void testHayEncuentroPeroSePasaron_2() {
		System.out.println("--> testHayEncuentroPeroSePasaron_2");
		Person p1 = new Person(10.0, 3, true);
		Person p2 = new Person(11.0, 1, true);
		test(p1, p2);
	}

	private static void testHayEncuentroPeroSePasaron() {
		System.out.println("--> testHayEncuentroPeroSePasaron");
		Person p1 = new Person(100.0, 1, true);
		Person p2 = new Person(101.0, 1, false);
		test(p1, p2);
	}

	private static void testHayEncuentro() {
		System.out.println("--> testHayEncuentro");
		Person p1 = new Person(100.0, 1, true);
		Person p2 = new Person(106.0, 1, false);
		test(p1, p2);
	}

	private static void testSeAlejan() {
		System.out.println("--> testSeAlejan");
		Person p1 = new Person(100.0, 1, false);
		Person p2 = new Person(105.0, 1, true);
		test(p1, p2);
	}

	private static void testAlgunoConPosicionNegativa() {
		System.out.println("--> testAlgunoConVelocidadNegativa");
		Person p1 = new Person(-100.0, 1, true);
		Person p2 = new Person(-102.0, 1, false);
		test(p1, p2);
	}

	private static void testAlgunoConVelocidadNegativa() {
		System.out.println("--> testAlgunoConVelocidadNegativa");
		Person p1 = new Person(100.0, -1, true);
		Person p2 = new Person(102.0, -1, false);
		test(p1, p2);
	}

	private static void testMismaPosicion() {
		System.out.println("--> testMismaPosicion");
		Person p1 = new Person(100.0, 1, true);
		Person p2 = new Person(100.0, 1, false);
		test(p1, p2);
	}

	private static void test(Person p1, Person p2) {
		JoinPointExample example = new JoinPointExample();
		System.out.println("Persona 1 :: ".concat(p1.toString()));
		System.out.println("Persona 2 :: ".concat(p2.toString()));
		try {
			double joinPoint = example.getJoinPoint(p1, p2);
			System.out.println(
					String.format("Las personas se encuentran en la posici�n %s respecto del punto A", joinPoint));
		} catch (RuntimeException e) {
			System.out.println("Error :: ".concat(e.getMessage()));
		}
	}

	public static class Person {
		double position;
		int velocity;
		boolean forward;

		public Person(double position, int velocity, boolean forward) {
			this.position = position;
			this.velocity = velocity;
			this.forward = forward;
		}

		@Override
		public String toString() {
			return "Person [position=" + position + ", velocity=" + velocity + ", forward=" + forward + "]";
		}

	}

}
