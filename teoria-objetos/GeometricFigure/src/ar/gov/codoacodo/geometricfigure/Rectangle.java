package ar.gov.codoacodo.geometricfigure;

public class Rectangle extends Square {

	private double side2;

	public double getSide2() {
		return side2;
	}

	public void setSide2(double side2) {
		this.side2 = side2;
	}

	@Override
	public double getSurface() {
		return side * side2;
	}

}
