package ar.gov.codoacodo.geometricfigure;

public class Square extends GeometricFigure {

	protected double side;

	public Square() {

	}

	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}

	@Override
	public double getSurface() {
		return side * side;
	}

}
