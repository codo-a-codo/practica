package ar.gov.codoacodo.geometricfigure;


/**
 * Representa la interface de una figura geometrica
 * @author juan.m.furattini
 *
 */
public abstract class GeometricFigure {

	/**
	 * Calcula la superficie de una figura geometrica
	 * 
	 * @return el valor de la superficie
	 */
	public abstract double getSurface();

}
