package ar.gov.codoacodo.geometricfigure;

public class Main {

	public static void main(String[] args) {
		testCalculoSuperficieCuadrado();
		testCalculoSuperficieRectangulo();
		testCalculoSuperficieCirculo();
	}

	private static void calculateGeometricFigureSurface(GeometricFigure geometricFigure) {
		System.out.println("Object class name: " + geometricFigure.getClass().getSimpleName());
		System.out.println("Geometric figure surface: " + geometricFigure.getSurface());
	}

	private static void testCalculoSuperficieCirculo() {
		Circle circle = new Circle();
		circle.setRadius(10.0);
		calculateGeometricFigureSurface(circle);
	}

	private static void testCalculoSuperficieRectangulo() {
		Rectangle rectangle = new Rectangle();
		rectangle.setSide(2.0);
		rectangle.setSide2(4.0);
		calculateGeometricFigureSurface(rectangle);
	}

	private static void testCalculoSuperficieCuadrado() {
		Square square = new Square();
		square.setSide(2.0);
		calculateGeometricFigureSurface(square);
	}

}
