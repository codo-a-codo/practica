package ar.gov.codoacodo.geometricfigure;

public class Circle extends GeometricFigure {

	private double radius;

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radio) {
		this.radius = radio;
	}

	@Override
	public double getSurface() {
		return Math.PI * Math.pow(radius, 2);
	}

}
