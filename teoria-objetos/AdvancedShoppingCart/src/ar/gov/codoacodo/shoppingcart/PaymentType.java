package ar.gov.codoacodo.shoppingcart;

public enum PaymentType {

	//@@formatter:off
	CASH("CS", "Efectivo"),
	DEBIT_CARD("DC", "Tarjeta de D�bito"),
	CREDIT_CARD("CC", "Tarjeta de Cr�dito"),
	CHECK("CH", "Cheque"),
	BANK_TRANSFER("BT", "Transferencia Bancaria"),
	PAYPAL("PP", "Paypal"),
	MERCADOPAGO("MP", "Mercado Pago");
	//@@formatter:on

	private final String code;
	private final String description;

	PaymentType(final String code, final String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static PaymentType getPaymentTypeByCode(String code) {
		for (PaymentType type : PaymentType.values()) {
			if (type.getCode().equals(code)) {
				return type;
			}
		}
		throw new RuntimeException("Invalid payment type");
	}

}
