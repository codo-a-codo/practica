package ar.gov.codoacodo.shoppingcart;

import java.util.Scanner;

public class Main {

	private static Scanner scanner;

	public static void main(String[] args) {

		scanner = new Scanner(System.in);

		System.out.print("Ingrese el monto a pagar: ");
		double total = scanner.nextDouble();
		System.out.print("Ingrese metodo de pago: ");
		String type = scanner.next();

		ShoppingCart cart = new ShoppingCart();
		cart.setTotalDue(total);
		cart.setPaymentType(type);
		cart.checkout();
	}
}
