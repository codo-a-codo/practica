package ar.gov.codoacodo.shoppingcart;

public class CashPayment extends Payment {

	public CashPayment(PaymentType type) {
		super(type);
	}

	@Override
	public void pay(double ammount) {
		displayPaymentMessage(ammount);
	}

}
