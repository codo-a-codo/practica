package ar.gov.codoacodo.shoppingcart;

import java.util.HashMap;
import java.util.Map;

public class PaymentFactory {

	private static PaymentFactory _instance = null;
	private static Map<PaymentType, Class<? extends Payment>> availablePaymentTypes = new HashMap<>();

	static {
		availablePaymentTypes.put(PaymentType.BANK_TRANSFER, BankTransferPayment.class);
		availablePaymentTypes.put(PaymentType.PAYPAL, PaypalPayment.class);
		availablePaymentTypes.put(PaymentType.CASH, CashPayment.class);
	}

	private PaymentFactory() {}

	public static final PaymentFactory getInstance() {
		if (_instance == null) {
			_instance = new PaymentFactory();
		}
		return _instance;
	}

	public Payment getPayment(PaymentType type) {
		Class<? extends Payment> payment = availablePaymentTypes.get(type);
		if (payment == null) {
			throw new RuntimeException("Payment type not allowed at this time.");
		}
		try {
			return payment.getConstructor(PaymentType.class).newInstance(type);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

}
