package ar.gov.codoacodo.shoppingcart;

public class PaypalPayment extends Payment {

	public PaypalPayment(PaymentType type) {
		super(type);
	}

	@Override
	public void pay(double ammount) {
		displayPaymentMessage(ammount);
	}

}
