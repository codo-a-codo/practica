package ar.gov.codoacodo.shoppingcart;

public class BankTransferPayment extends Payment {

	public BankTransferPayment(PaymentType type) {
		super(type);
	}

	@Override
	public void pay(double ammount) {
		displayPaymentMessage(ammount);
	}

}
