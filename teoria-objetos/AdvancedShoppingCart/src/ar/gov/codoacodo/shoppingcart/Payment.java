package ar.gov.codoacodo.shoppingcart;

public abstract class Payment {

	protected static final String PAYMENT_MESSAGE = "Realizando un pago a trav�s de %s por un total de $%s";

	protected final PaymentType type;

	public Payment(PaymentType type) {
		this.type = type;
	}

	public PaymentType getType() {
		return type;
	}

	public abstract void pay(double ammount);

	protected void displayPaymentMessage(double ammount) {
		System.out.println(String.format(PAYMENT_MESSAGE, type.getDescription(), ammount));
	}

}
