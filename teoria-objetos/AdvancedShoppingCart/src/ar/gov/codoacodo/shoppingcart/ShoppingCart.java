package ar.gov.codoacodo.shoppingcart;

public class ShoppingCart {

	private String paymentType = "EFT";
	private double totalDue = 0.0;

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public double getTotalDue() {
		return totalDue;
	}

	public void setTotalDue(double totalDue) {
		this.totalDue = totalDue;
	}

	public void checkout() {
		try {
			PaymentType type = PaymentType.getPaymentTypeByCode(this.paymentType);
			Payment payment = PaymentFactory.getInstance().getPayment(type);
			doPayment(payment);
		} catch (RuntimeException e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

	private void doPayment(Payment payment) {
		payment.pay(totalDue);
	}

}
