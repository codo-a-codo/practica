package ar.gov.codoacodo.demojava.clases;

public enum TipoDocumento {

	LC, LE, DNI, PP, CD;

}
