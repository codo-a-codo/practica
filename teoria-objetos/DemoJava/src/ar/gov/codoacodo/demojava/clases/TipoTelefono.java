package ar.gov.codoacodo.demojava.clases;

public enum TipoTelefono {

	FIJO, MOVIL, FAX, OTRO;

}
