package ar.gov.codoacodo.demojava.clases;

public class DatosDeContacto {

	private Domicilio domicilio;
	private Telefono telefono;
	private String mail;

	public Domicilio getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(Domicilio domicilio) {
		this.domicilio = domicilio;
	}

	public Telefono getTelefono() {
		return telefono;
	}

	public void setTelefono(Telefono telefono1) {
		this.telefono = telefono1;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

}
