package ar.gov.codoacodo.demojava.clases;

public class Documento {

	private TipoDocumento tipo;
	private int numero;

	public TipoDocumento getTipo() {
		return tipo;
	}

	public void setTipo(TipoDocumento tipo) {
		this.tipo = tipo;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return tipo + " " + numero;
	}

}
