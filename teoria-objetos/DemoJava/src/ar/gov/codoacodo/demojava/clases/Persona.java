package ar.gov.codoacodo.demojava.clases;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ar.gov.codoacodo.demojava.utils.DateUtils;

public class Persona {

	// Atributos
	private Date fechaNacimiento;
	private Nombres nombres;
	private Documento documento;
	private Genero genero;
	private List<DatosDeContacto> datosPersonales = new ArrayList<>();

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public Nombres getNombres() {
		return nombres;
	}

	public void setNombres(Nombres nombre) {
		this.nombres = nombre;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public List<DatosDeContacto> getDatosDeContacto() {
		return datosPersonales;
	}

	public void setDatosDeContacto(List<DatosDeContacto> datosDeContacto) {
		this.datosPersonales = datosDeContacto;
	}

	public void agregarDatosPersonales(DatosDeContacto datosPersonales) {
		this.datosPersonales.add(datosPersonales);
	}

	public int getEdad() {
		return DateUtils.getAniosAHoy(fechaNacimiento);
	}

}
