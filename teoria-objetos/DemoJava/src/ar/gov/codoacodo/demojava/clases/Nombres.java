package ar.gov.codoacodo.demojava.clases;

public class Nombres {

	private String primerNombre;
	private String segundoNombre;
	private String primerApellido;
	private String segundoApellido;

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	@Override
	public String toString() {
		String nombreCompleto = "";
		nombreCompleto += agregarAlNombre(primerNombre);
		nombreCompleto += agregarAlNombre(segundoNombre);
		nombreCompleto += agregarAlNombre(primerApellido);
		nombreCompleto += agregarAlNombre(segundoApellido);
		return nombreCompleto.trim();
	}

	private String agregarAlNombre(String palabra) {
		return palabra != null ? " " + palabra : "";
	}

}
