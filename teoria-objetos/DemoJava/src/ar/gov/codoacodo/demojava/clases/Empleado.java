package ar.gov.codoacodo.demojava.clases;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ar.gov.codoacodo.demojava.utils.DateUtils;

public class Empleado extends Persona {

	public static final int CUIT_EMPRESA = 123;

	private int legajo;
	private Date fechaIngreso;
	private List<DatosDeContacto> datosLaborales = new ArrayList<>();

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int numero) {
		this.legajo = numero;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public List<DatosDeContacto> getDatosLaborales() {
		return datosLaborales;
	}

	public void setDatosLaborales(List<DatosDeContacto> datosLaborales) {
		this.datosLaborales = datosLaborales;
	}

	public void agregarDatosLaborales(DatosDeContacto datosLaborales) {
		this.datosLaborales.add(datosLaborales);
	}

	public int getAntiguedad() {
		return DateUtils.getAniosAHoy(fechaIngreso);
	}

}
