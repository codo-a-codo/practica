package ar.gov.codoacodo.demojava;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import ar.gov.codoacodo.demojava.clases.DatosDeContacto;
import ar.gov.codoacodo.demojava.clases.Documento;
import ar.gov.codoacodo.demojava.clases.Domicilio;
import ar.gov.codoacodo.demojava.clases.Empleado;
import ar.gov.codoacodo.demojava.clases.Genero;
import ar.gov.codoacodo.demojava.clases.Nombres;
import ar.gov.codoacodo.demojava.clases.Telefono;
import ar.gov.codoacodo.demojava.clases.TipoDocumento;
import ar.gov.codoacodo.demojava.clases.TipoTelefono;

// Pascal Case o Upper Camel Case
public class DemoJava {

	private static final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

	public static void main(String[] args) {

		// DATOS DEL EMPLEADO
		Empleado empleado = new Empleado();

		//// Legajo
		empleado.setLegajo(123456);

		//// Fecha de Ingreso
		Calendar calendarIngreso = Calendar.getInstance();
		calendarIngreso.set(Calendar.YEAR, 2008);
		calendarIngreso.set(Calendar.MONTH, 8);
		calendarIngreso.set(Calendar.DAY_OF_MONTH, 1);
		empleado.setFechaIngreso(calendarIngreso.getTime());

		//// Datos de Contacto Laboral
		DatosDeContacto datosLaborales1 = new DatosDeContacto();
		////// Telefono
		Telefono telefonoLaboral1 = new Telefono();
		telefonoLaboral1.setCodigoArea(11);
		telefonoLaboral1.setInterno(12);
		telefonoLaboral1.setNumero(44445555);
		telefonoLaboral1.setTipo(TipoTelefono.FIJO);
		datosLaborales1.setTelefono(telefonoLaboral1);
		////// Mail
		datosLaborales1.setMail("juan.tito@empresa.com");
		////// Domicilio
		Domicilio domicilioLaboral1 = new Domicilio();
		domicilioLaboral1.setProvincia("Buenos Aires");
		domicilioLaboral1.setLocalidad("Capital Federal");
		domicilioLaboral1.setCodigoPostal(1409);
		domicilioLaboral1.setCalle("Marcelo T de Alvear");
		domicilioLaboral1.setAltura(888);
		domicilioLaboral1.setPiso(8);
		datosLaborales1.setDomicilio(domicilioLaboral1);
		//
		empleado.agregarDatosLaborales(datosLaborales1);

		// Fecha de nacimiento
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 1987);
		calendar.set(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, 2);
		empleado.setFechaNacimiento(calendar.getTime());

		// Nombres
		Nombres nombres = new Nombres();
		nombres.setPrimerNombre("Juan");
		nombres.setSegundoNombre("Manuel");
		nombres.setPrimerApellido("Furattini");
		empleado.setNombres(nombres);

		// Documento
		Documento documento = new Documento();
		documento.setTipo(TipoDocumento.DNI);
		documento.setNumero(27099021);
		empleado.setDocumento(documento);

		// Genero
		empleado.setGenero(Genero.MASCULINO);

		//// Datos de Contacto Personal
		DatosDeContacto datosPersonales1 = new DatosDeContacto();
		////// Telefono
		Telefono telefonoPersonal1 = new Telefono();
		telefonoPersonal1.setCodigoArea(11);
		telefonoPersonal1.setInterno(12);
		telefonoPersonal1.setNumero(44445555);
		telefonoPersonal1.setTipo(TipoTelefono.FIJO);
		datosPersonales1.setTelefono(telefonoPersonal1);
		////// Mail
		datosPersonales1.setMail("juan.tito@empresa.com");
		////// Domicilio
		Domicilio domicilioPersonal1 = new Domicilio();
		domicilioPersonal1.setProvincia("Buenos Aires");
		domicilioPersonal1.setLocalidad("Capital Federal");
		domicilioPersonal1.setCodigoPostal(1409);
		domicilioPersonal1.setCalle("Marcelo T de Alvear");
		domicilioPersonal1.setAltura(888);
		domicilioPersonal1.setPiso(8);
		datosPersonales1.setDomicilio(domicilioPersonal1);
		//
		// empleado.agregarDatosPersonales(datosPersonales1);

		imprimirPlanillaEmpleado(empleado);

	}

	private static void imprimirPlanillaEmpleado(Empleado empleado) {

		System.out.println("============================================");
		System.out.println("Datos del empleado:");
		System.out.println("============================================");
		System.out.println("Legajo: " + empleado.getLegajo());
		System.out.println("Nombre: " + empleado.getNombres().toString());
		System.out.println("Sexo: " + empleado.getGenero());
		System.out.println("Fecha de nacimiento: " + format.format(empleado.getFechaNacimiento()) + " ("
				+ empleado.getEdad() + " a�os)");
		System.out.println("Documento: " + empleado.getDocumento().toString());
		System.out.println("Fecha de ingreso: " + format.format(empleado.getFechaIngreso()) + " (antiguedad: "
				+ empleado.getAntiguedad() + " a�os)");

		System.out.println("");
		System.out.println("============================================");
		System.out.println("Datos de contacto:");
		System.out.println("============================================");
		System.out.println("Pesonales:");
		System.out.println("Laborales:");

	}

}
