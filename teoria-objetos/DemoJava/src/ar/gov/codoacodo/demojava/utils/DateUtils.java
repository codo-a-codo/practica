package ar.gov.codoacodo.demojava.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	public static int getAniosAHoy(Date fechaInicio) {
		return getDiferenciaEnAnios(fechaInicio, new Date());
	}

	public static int getDiferenciaEnAnios(Date fechaInicio, Date fechaFin) {
		Calendar inicio = dateToCalendar(fechaInicio);
		Calendar fin = dateToCalendar(fechaFin);
		int diferencia = fin.get(Calendar.YEAR) - inicio.get(Calendar.YEAR);
		diferencia -= (fin.get(Calendar.DAY_OF_YEAR) - inicio.get(Calendar.DAY_OF_YEAR)) < 0 ? 1 : 0;
		return diferencia;
	}

	public static Calendar dateToCalendar(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}

}
