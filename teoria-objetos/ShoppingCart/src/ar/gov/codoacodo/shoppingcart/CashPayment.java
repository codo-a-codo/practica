package ar.gov.codoacodo.shoppingcart;

public class CashPayment extends Payment {

	@Override
	public void pay(double ammount) {
		System.out.println("Efectuando el pago en efectivo por un total de " + ammount);
	}

}
