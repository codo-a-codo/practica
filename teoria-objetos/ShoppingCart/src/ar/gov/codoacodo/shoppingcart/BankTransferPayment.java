package ar.gov.codoacodo.shoppingcart;

public class BankTransferPayment extends Payment {

	@Override
	public void pay(double ammount) {
		System.out.println("Efectuando el pago a trav�s de transferencia bancaria por un total de " + ammount);
	}

}
