package ar.gov.codoacodo.shoppingcart;

public abstract class Payment {

	public abstract void pay(double ammount);

}
