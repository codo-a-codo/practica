package ar.gov.codoacodo.shoppingcart;

public class PaypalPayment extends Payment {

	@Override
	public void pay(double ammount) {
		System.out.println("Efectuando el pago a trav�s de paypal por un total de " + ammount);
	}

}
