package ar.gov.codoacodo.shoppingcart;

public class Main {

	public static void main(String[] args) {
		testPagoEnEfectivo();
		testPagoConPaypal();
		testPagoConTransferenciaBancaria();
	}

	private static void testPagoConTransferenciaBancaria() {
		ShoppingCart cart = new ShoppingCart();
		cart.setTotalDue(220.0);
		cart.setPayment(new BankTransferPayment());
		cart.checkout();
	}

	private static void testPagoConPaypal() {
		ShoppingCart cart = new ShoppingCart();
		cart.setTotalDue(220.0);
		cart.setPayment(new PaypalPayment());
		cart.checkout();
	}

	private static void testPagoEnEfectivo() {
		ShoppingCart cart = new ShoppingCart();
		cart.setTotalDue(220.0);
		cart.setPayment(new CashPayment());
		cart.checkout();
	}

}
