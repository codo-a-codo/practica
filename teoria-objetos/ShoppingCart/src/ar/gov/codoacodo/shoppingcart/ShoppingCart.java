package ar.gov.codoacodo.shoppingcart;

public class ShoppingCart {

	private Payment payment;
	private double totalDue = 0.0;

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public double getTotalDue() {
		return totalDue;
	}

	public void setTotalDue(double totalDue) {
		this.totalDue = totalDue;
	}

	public void checkout() {
		payment.pay(totalDue);
	}

}
